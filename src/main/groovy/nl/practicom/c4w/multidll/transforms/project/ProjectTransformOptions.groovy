package nl.practicom.c4w.multidll.transforms.project

import nl.practicom.c4w.multidll.transforms.application.ApplicationType

class ProjectTransformOptions{
    String assemblyName
    String outputName
    ApplicationType applicationType
}
