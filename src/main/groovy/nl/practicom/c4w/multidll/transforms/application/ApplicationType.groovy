package nl.practicom.c4w.multidll.transforms.application

enum ApplicationType {
    MainApplication, ProcedureDLL, DataDLL
}